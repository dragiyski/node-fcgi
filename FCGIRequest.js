(function () {
    'use strict';

    const stream = require('stream');
    const { EventEmitter } = require('events');

    const drainStream = require('./drainStream');
    const TCPAlignStream = require('./TCPAlignStream');
    const FCGIPacketStream = require('./FCGIPacketStream');
    const FCGIParamStream = require('./FCGIParamStream');
    const FCGIOutputStream = require('./FCGIOutputStream');

    const constants = require('./constants');

    const symbols = {
        options: Symbol('options'),
        internalStreams: Symbol('internalStreams'),
        outputStream: Symbol('outputStream'),
        inputStream: Symbol('inputStream'),
        socket: Symbol('socket'),
        finished: Symbol('finished'),
        listeners: Symbol('listeners'),
        _connect: Symbol('_connect'),
        _send: Symbol('_send'),
        _recv: Symbol('_recv'),
        _sendBeginRequest: Symbol('_sendBeginRequest'),
        _streamParams: Symbol('_streamParams'),
        _streamData: Symbol('_streamData'),
        _on: Symbol('_on'),
        _once: Symbol('_once'),
        _cleanup: Symbol('_cleanup')
    };

    class FCGIRequest extends EventEmitter {
        constructor(socket, options) {
            super();
            options = { ...options };

            if (socket.ended || socket.destroyed) {
                throw new Error('The provided socket is already closed or destroyed');
            }

            this[symbols.internalStreams] = new Set();

            if (options.id == null) {
                options.id = 1;
            } else {
                options.id = parseInt(options.id);
            }
            validateInteger('id', options.id, 1, 65535);
            if (options.role == null) {
                options.role = constants.fcgiRoles.FCGI_RESPONDER;
            } else {
                options.role = parseInt(options.role);
            }
            validateInteger('role', options.role, 1, constants.fcgiRoles.FCGI_MAX_ROLE);
            if (options.params == null) {
                options.params = {};
            } else if (typeof options.params !== 'object') {
                throw new TypeError('Option [params] must be object or null');
            }
            const inputStreamList = ['stdin'];
            if (options.role === constants.fcgiRoles.FCGI_FILTER) {
                inputStreamList.push('data');
            }
            for (const streamType of inputStreamList) {
                if (options[streamType] == null) {
                    options[streamType] = createEmptyStream();
                    this[symbols.internalStreams].add(options[streamType]);
                }
                if (!(options[streamType] instanceof stream.Readable) || !options[streamType].readable || options[streamType].readableObjectMode) {
                    throw new TypeError(`Option [${streamType}] must be readable stream in non-object mode`);
                }
            }
            for (const channel of ['stdout', 'stderr']) {
                if (options[channel] == null) {
                    options[channel] = createVoidStream();
                    this[symbols.internalStreams].add(options[channel]);
                }
                if (!(options[channel] instanceof stream.Writable) || !options[channel].writable || options[channel].writableObjectMode) {
                    throw new TypeError(`Option [${channel}] must be readable stream in non-object mode`);
                }
            }
            this[symbols.options] = options;
            this[symbols.socket] = socket;
            this[symbols.inputStream] = new TCPAlignStream();
            this[symbols.outputStream] = new FCGIOutputStream(async packet => {
                return void await this[symbols._recv](packet, this[symbols.outputStream]);
            }, () => {
            });
            this[symbols.internalStreams].add(this[symbols.inputStream]);
            this[symbols.internalStreams].add(this[symbols.outputStream]);
            this[symbols.listeners] = [];
            for (const stream of this[symbols.internalStreams]) {
                this[symbols._once](stream, 'error', error => {
                    this.error(error, stream);
                });
            }
            this[symbols._on](this[symbols.inputStream], 'data', data => {
                this.emit('tcp.send', data);
            });
            this[symbols.inputStream].pipe(socket);
            // Output side pipes to output stream, which then calls [_recv]
            this[symbols._once](socket, 'error', error => {
                this.error(error, socket);
            })[symbols._on](socket, 'data', data => {
                this.emit('tcp.recv', data);
            });
            socket.pipe(this[symbols.outputStream]);
            if (!this[symbols.options].multiplex) {
                this[symbols.internalStreams].add(socket);
            }
            this[symbols._send]().catch(error => {
                this.error(error);
            });
        }

        error(error, stream = null) {
            for (const listener of this[symbols.listeners]) {
                listener.emitter.off(listener.event, listener.callback);
            }
            for (const stream of this[symbols.internalStreams]) {
                if (!stream.destroyed) {
                    // NodeJS can potentially throw error on next tick, even after we have cleaned up
                    stream.on('error', () => {});
                    stream.destroy();
                }
            }
            this.emit('error', error);
        }

        [symbols._on](emitter, event, callback) {
            this[symbols.listeners].push({
                emitter,
                event,
                callback
            });
            emitter.on(event, callback);
            return this;
        }

        [symbols._once](emitter, event, callback) {
            this[symbols.listeners].push({
                emitter,
                event,
                callback
            });
            emitter.once(event, callback);
            return this;
        }

        async [symbols._send]() {
            if (await this[symbols._connect]() &&
                await this[symbols._sendBeginRequest]() &&
                await this[symbols._streamParams]() &&
                await this[symbols._streamData](constants.fcgiTypes.FCGI_STDIN, this[symbols.options].stdin) &&
                (
                    this[symbols.options].role === constants.fcgiRoles.FCGI_FILTER
                        ? await this[symbols._streamData](constants.fcgiTypes.FCGI_DATA, this[symbols.options].data)
                        : true
                )) {
                this[symbols.inputStream].end();
            }
        }

        async [symbols._recv](packet, stream) {
            this.emit('recv', packet.buffer);
            if (packet.ignore || packet.requestId !== this[symbols.options].id) {
                return;
            }
            if (packet.type === constants.fcgiTypes.FCGI_STDOUT || packet.type === constants.fcgiTypes.FCGI_STDERR) {
                const streamType = packet.type === constants.fcgiTypes.FCGI_STDOUT ? 'stdout' : 'stderr';
                const stream = this[symbols.options][streamType];
                if (packet.contentLength > 0) {
                    if (!stream.write(packet.buffer.slice(constants.FCGI_HEADER_SIZE, constants.FCGI_HEADER_SIZE + packet.contentLength))) {
                        await drainStream(stream);
                    }
                } else {
                    this.emit(`${streamType}.end`);
                }
            } else if (packet.type === constants.fcgiTypes.FCGI_END_REQUEST) {
                const result = {
                    appStatus: packet.buffer.readUInt32BE(constants.FCGI_HEADER_SIZE),
                    protocolStatus: packet.buffer.readUInt8(constants.FCGI_HEADER_SIZE + 4)
                };
                this[symbols.finished] = true;
                this.emit('end', result);
                this[symbols._cleanup]();
            }
        }

        async [symbols._connect]() {
            if (this[symbols.socket].pending) {
                return new Promise((resolve) => {
                    const onConnect = () => {
                        this[symbols.socket].off('error', onError);
                        resolve(true);
                    };
                    const onError = () => {
                        this[symbols.socket].off('connect', onConnect);
                        resolve(false);
                    };
                    this[symbols.socket].once('connect', onConnect).once('error', onError);
                });
            }
            return true;
        }

        async [symbols._sendBeginRequest]() {
            const buffer = Buffer.alloc(constants.FCGI_BEGIN_REQUEST_SIZE + constants.FCGI_HEADER_SIZE);
            buffer.writeUInt8(constants.FCGI_VERSION, 0);
            buffer.writeUInt8(constants.fcgiTypes.FCGI_BEGIN_REQUEST, 1);
            buffer.writeUInt16BE(this[symbols.options].id, 2);
            buffer.writeUInt16BE(constants.FCGI_BEGIN_REQUEST_SIZE, 4);
            buffer.writeUInt16BE(0, 6);
            buffer.writeUInt16BE(this[symbols.options].role, 8);
            buffer.writeUInt16BE(this[symbols.options].multiplex ? 1 : 0, 10);
            this.emit('send', buffer);
            if (!this[symbols.inputStream].write(buffer)) {
                return drainStream(this[symbols.inputStream]).then(() => true, error => {
                    this.error(error, this[symbols.inputStream]);
                    return false;
                });
            }
            return true;
        }

        async [symbols._streamParams]() {
            const parameterStream = new FCGIParamStream(this[symbols.options].params);
            const packetStream = new FCGIPacketStream({
                type: constants.fcgiTypes.FCGI_PARAMS,
                requestId: this[symbols.options].id
            });
            parameterStream.pipe(packetStream).pipe(this[symbols.inputStream], { end: false });
            this[symbols.internalStreams].add(parameterStream);
            this[symbols.internalStreams].add(packetStream);
            return new Promise((resolve, reject) => {
                this[symbols._once](parameterStream, 'error', error => {
                    this.error(error, parameterStream);
                    resolve(false);
                });
                this[symbols._once](packetStream, 'error', error => {
                    this.error(error, packetStream);
                    resolve(false);
                })[symbols._once](packetStream, 'end', () => {
                    resolve(true);
                })[symbols._on](packetStream, 'data', data => {
                    this.emit('send', data);
                });
            });
        }

        async [symbols._streamData](type, stream) {
            const packetStream = new FCGIPacketStream({
                type,
                requestId: this[symbols.options].id
            });
            stream.pipe(packetStream).pipe(this[symbols.inputStream], { end: false });
            this[symbols.internalStreams].add(packetStream);
            return new Promise((resolve, reject) => {
                this[symbols._once](packetStream, 'error', error => {
                    this.error(error, packetStream);
                    resolve(false);
                })[symbols._once](packetStream, 'end', () => {
                    resolve(true);
                })[symbols._on](packetStream, 'data', data => {
                    this.emit('send', data);
                });
            });
        }

        [symbols._cleanup]() {
            const actualCleanup = () => {
                for (const listener of this[symbols.listeners]) {
                    listener.emitter.off(listener.event, listener.callback);
                }
                for (const stream of this[symbols.internalStreams]) {
                    if (!stream.destroyed) {
                        stream.on('error', () => {});
                        stream.destroy();
                    }
                }
            };
            if (!this[symbols.options].multiplex) {
                // If we are not multiplexing, we close the FCGI connection and then cleanup...
                this[symbols.socket].end(() => {
                    this[symbols.socket].destroy();
                    actualCleanup();
                });
            } else {
                // Otherwise, just destroy everything...
                actualCleanup();
            }
        }

        get stdin() {
            return this[symbols.options].stdin;
        }

        get stdout() {
            return this[symbols.options].stdout;
        }

        get stderr() {
            return this[symbols.options].stderr;
        }

        get data() {
            return this[symbols.options].data;
        }

        get role() {
            return this[symbols.options].role;
        }

        get id() {
            return this[symbols.options].id;
        }

        get isMultiplex() {
            return this[symbols.options].multiplex;
        }

        get socket() {
            return this[symbols.socket];
        }
    }

    function createEmptyStream() {
        return new stream.Readable({
            read(size) {
                this.push(null);
            }
        });
    }

    function createVoidStream() {
        return new stream.Writable({
            write(chunk, encoding, callback) {
                callback();
            }
        });
    }

    function validateInteger(option, number, min, max) {
        if (number !== parseInt(number)) {
            throw new TypeError(`Expected option [${option}] to be an integer number`);
        }
        if (number < min || number > max) {
            throw new RangeError(`Expected option [${option}] to be in range [${min}; ${max}]`);
        }
    }

    module.exports = FCGIRequest;
})();
