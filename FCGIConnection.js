(function () {
    'use strict';

    const events = require('events');
    const http2 = require('http2');
    const net = require('net');
    const path = require('path');
    const stream = require('stream');
    const url = require('url');

    const FCGIRequest = require('./FCGIRequest');
    const HTTPStream = require('./HTTPStream');
    const IDGenerator = require('./IDGenerator');

    const symbols = {
        options: Symbol('options'),
        error: Symbol('error'),
        closed: Symbol('closed'),
        _connect: Symbol('_connect'),
        _sendBeginRequest: Symbol('_sendBeginRequest'),
        _streamParams: Symbol('_streamParams'),
        _streamData: Symbol('_streamData'),
        _recvPackets: Symbol('_recvPackets'),
        _loadHeaders: Symbol('_loadHeaders')
    };

    const defaultOptions = {
        alignment: 8,
        encoding: 'utf8'
    };

    class FCGIConnection extends events.EventEmitter {
        constructor(options = {}) {
            super();
            options = { ...options };
            this[symbols.options] = { ...defaultOptions };
            if (typeof options.path === 'string' && options.path.length > 0) {
                if (typeof options.host === 'string' && options.host.length > 0) {
                    throw new TypeError('Incompatible set of options: both [path] and [host] options specified');
                }
                this[symbols.options].path = options.path;
            } else if (typeof options.host === 'string' && options.host.length > 0) {
                if (typeof options.port === 'string') {
                    options.port = parseInt(options.port);
                }
                if (typeof options.port !== 'number' || !isFinite(options.port) || options.port !== parseInt(options.port) || options.port <= 0 || options.port >= 65536) {
                    throw new TypeError('Invalid option: if [host] is specified, [port] must be integer (or integer string) between 1 and 65535');
                }
                this[symbols.options].host = options.host;
                this[symbols.options].port = options.port;
            } else {
                throw new TypeError('Invalid set of options: exactly one of [path] or [host] must be specified');
            }
            for (const optionName in defaultOptions) {
                if (Object.hasOwnProperty.call(defaultOptions, optionName) && Object.hasOwnProperty.call(options, optionName)) {
                    this[symbols.options][optionName] = options[optionName];
                }
            }
        }

        request(options = {}) {
            return new FCGIRequest(this[symbols._connect](), options);
        }

        http(request, response, options = {}) {
            options = { ...options };
            if (typeof options.path !== 'string' || options.path.length <= 0 || !path.isAbsolute(options.path)) {
                throw new Error('Expected option [path] to be absolute filesystem path', 'path');
            }
            if (typeof options.documentRoot !== 'string' || options.documentRoot.length <= 0 || !path.isAbsolute(options.documentRoot)) {
                throw new Error('Expected option [documentRoot] to be absolute filesystem path', 'documentRoot');
            }
            if (options.alias != null) {
                if (typeof options.alias !== 'object') {
                    throw new Error('Expected option [alias] to be an object', 'alias');
                }
                if (typeof options.alias.prefix !== 'string' || !options.alias.prefix.startsWith('/')) {
                    throw new Error('Expected option [alias.prefix] to be string starting with slash (/)', 'alias.prefix');
                }
                if (typeof options.alias.documentRoot !== 'string' || options.alias.documentRoot.length <= 0 || !path.isAbsolute(options.alias.documentRoot)) {
                    throw new Error('Expected option [alias.documentRoot] to be absolute filesystem path', 'alias.documentRoot');
                }
            }
            const baseUrl = url.format({
                protocol: request.socket.encrypted ? 'https:' : 'http:',
                slashes: true,
                host: request.authority || request.headers && request.headers.host || (() => {
                    const addressUrl = {
                        hostname: getIP(request.socket.localAddress)
                    };
                    if (request.socket.localPort !== (request.socket.encrypted ? 443 : 80)) {
                        addressUrl.port = request.socket.localPort;
                    }
                    return url.format(addressUrl);
                })
            });
            const requestUrl = new url.URL(request.url, baseUrl);
            const parameters = {};
            if (options.http2 !== false && (request.httpVersionMajor >= 2 || options.http2)) {
                this.loadHttp2Parameters(parameters, request, options.http2);
            }
            if (options.ssl !== false && (request.socket.encrypted || options.ssl)) {
                this.loadSSLParameters(parameters, request, options.ssl);
            }
            if (options.serverSoftware != null) {
                parameters.SERVER_SOFTWARE = options.serverSoftware;
            } else if (options.serverSoftware !== false) {
                parameters.SERVER_SOFTWARE = `NodeJS/${process.version}`;
            }
            if (options.serverSignature != null) {
                parameters.SERVER_SIGNATURE = options.serverSignature;
            }
            if (options.serverAdmin != null) {
                parameters.SERVER_ADMIN = options.serverAdmin;
            }
            // Host header no longer exists in HTTP2, instead authority is used...
            // In Apache there is subtle difference where SERVER_NAME is a value in the config files, however
            // in order a VirtualHost to be called, HTTP_HOST must match the SERVER_NAME
            // For this implementation it makes no difference and HTTP_HOST is always set to the SERVER_NAME
            parameters.HTTP_HOST = parameters.SERVER_NAME = requestUrl.host;
            parameters.SERVER_ADDR = getIP(request.socket.localAddress);
            parameters.SERVER_PORT = request.socket.localPort;
            parameters.REMOTE_ADDR = getIP(request.socket.remoteAddress);
            parameters.REMOTE_PORT = request.socket.remotePort;
            if (options.envPath !== false) {
                parameters.PATH = options.envPath != null ? options.envPath : process.env.PATH;
            }
            parameters.SCRIPT_FILENAME = this.getScriptFilename(options.path);
            parameters.DOCUMENT_ROOT = options.documentRoot;
            if (options.alias != null) {
                parameters.CONTEXT_PREFIX = options.alias.prefix;
                parameters.CONTEXT_DOCUMENT_ROOT = options.alias.documentRoot;
            } else {
                parameters.CONTEXT_PREFIX = '';
                parameters.CONTEXT_DOCUMENT_ROOT = options.documentRoot;
            }
            parameters.GATEWAY_INTERFACE = 'CGI/1.1';
            parameters.SERVER_PROTOCOL = `HTTP/${request.httpVersion}`;
            parameters.REQUEST_METHOD = request.method;
            parameters.QUERY_STRING = requestUrl.search;
            if (parameters.QUERY_STRING.startsWith('?')) {
                parameters.QUERY_STRING = parameters.QUERY_STRING.substr(1);
            }
            parameters.REQUEST_SCHEME = requestUrl.protocol;
            if (parameters.REQUEST_SCHEME.endsWith(':')) {
                parameters.REQUEST_SCHEME = parameters.REQUEST_SCHEME.substr(0, parameters.REQUEST_SCHEME.length - 1);
            }
            parameters.REQUEST_URI = request.url;
            if (options.scriptName !== false) {
                parameters.SCRIPT_NAME = options.scriptName != null ? options.scriptName : requestUrl.pathname;
            }
            const requestHeaders = { ...request.headers };
            if (options.requestHeaders != null && typeof options.requestHeaders === 'object') {
                this[symbols._loadHeaders](requestHeaders, options.requestHeaders);
            }
            for (let headerName in requestHeaders) {
                if (Object.hasOwnProperty.call(requestHeaders, headerName)) {
                    if (headerName.startsWith(':')) {
                        // Ignore HTTP2 special headers
                        continue;
                    }
                    const headerValue = requestHeaders[headerName];
                    headerName = headerName.split('-').join('_').toUpperCase();
                    if (headerName === 'CONTENT_TYPE' || headerName === 'CONTENT_LENGTH') {
                        parameters[headerName] = headerValue;
                    } else {
                        parameters[`HTTP_${headerName}`] = headerValue;
                    }
                }
            }
            const responseHeaders = {};
            const httpStream = new HTTPStream();
            const fcgiRequest = this.request({
                params: parameters,
                stdin: request,
                stdout: httpStream,
                stderr: options.stderr
            });
            httpStream.once('error', error => void fcgiRequest.error(error));
            httpStream.once('status', (code, message) => {
                response.statusCode = code;
                if (request.httpVersionMajor < 2 && typeof message === 'string' && message.length > 0) {
                    response.statusMessage = message;
                }
            });
            httpStream.on('header', (name, value) => {
                if (!Object.hasOwnProperty.call(responseHeaders, name)) {
                    responseHeaders[name] = value;
                } else {
                    if (!Array.isArray(responseHeaders[name])) {
                        responseHeaders[name] = [responseHeaders[name]];
                    }
                    responseHeaders[name].push(value);
                }
            });
            httpStream.once('body', () => {
                if (!response.headersSent) {
                    const headers = { ...responseHeaders };
                    if (options.responseHeaders != null && typeof options.responseHeaders === 'object') {
                        this[symbols._loadHeaders](headers, options.responseHeaders);
                    }
                    for (const headerName in headers) {
                        if (Object.hasOwnProperty.call(headers, headerName)) {
                            response.setHeader(headerName, headers[headerName]);
                        }
                    }
                }
            });
            httpStream.pipe(response);
            fcgiRequest.once('error', error => {
                if (!response.headersSent && response.writable) {
                    if (!this.emit('requestError', error, request, response)) {
                        const text = error.stack || error + '';
                        response.statusCode = 502;
                        if (options.showError) {
                            response.setHeader('content-type', 'text/plain;charset=utf-8');
                            response.setHeader('content-length', Buffer.byteLength(text, 'utf8'));
                            response.end(text, 'utf8');
                        } else {
                            response.end();
                        }
                        if (this.listenerCount('error') > 0) {
                            this.emit('error', error);
                        } else {
                            process.stderr.write(text + '\n', 'utf8');
                        }
                    }
                }
                httpStream.destroy();
            });
            const onResponseEnd = () => {
                if (response.writable && options['stdout.end'] !== false) {
                    response.end();
                }
            };
            const onErrorEnd = () => {
                if (options.stderr instanceof stream.Stream && options.stderr.writable && options.stderr !== process.stderr && options['stderr.end'] === true) {
                    options.stderr.end();
                }
            };
            fcgiRequest.once('stdout.end', onResponseEnd).once('end', onResponseEnd);
            fcgiRequest.once('stderr.end', onErrorEnd).once('end', onErrorEnd);
            return fcgiRequest;
        }

        loadHttp2Parameters(parameters, request, options = {}) {
            options = { ...options };
            parameters.HTTP2 = 'on';
            const defaults = {
                isPush: false,
                pushedOn: null
            };
            defineCacheGetter(defaults, 'pushAllowed', () => {
                return request.stream && request.stream.pushAllowed;
            });
            defineCacheGetter(defaults, 'sessionId', () => {
                // Non-HTTP2 request has one request per connection
                if (!request.stream || !request.stream.session) {
                    return null;
                }
                if (symbols.id in request.stream.session) {
                    return request.stream.session[symbols.id];
                }
                if (!(http2[symbols.id] instanceof IDGenerator)) {
                    http2[symbols.id] = new IDGenerator();
                }
                return request.stream.session[symbols.id] = http2[symbols.id].next();
            });
            defineCacheGetter(defaults, 'streamId', () => {
                return request.stream && request.stream.id || 1;
            });
            defineCacheGetter(defaults, 'streamTag', function () {
                return this.sessionId != null && this.streamId != null ? this.sessionId + '-' + this.streamId : null;
            });
            Object.setPrototypeOf(options, defaults);
            if (options.pushAllowed != null) {
                parameters.H2PUSH = parameters.H2_PUSH = options.pushAllowed;
            }
            if (options.isPush != null && options.streamId != null) {
                parameters.H2_PUSHED = options.isPush ? 'PUSHED' : '';
                parameters.H2_PUSHED_ON = options.isPush ? options.streamId : '';
            }
            if (options.streamId != null) {
                parameters.H2_STREAM_ID = options.streamId;
            }
            if (options.streamTag != null) {
                parameters.H2_STREAM_TAG = options.streamTag;
            }
            return this;
        }

        loadSSLParameters(parameters, request, options = {}) {
            options = { ...options };
            parameters.HTTPS = 'on';
            const defaults = {
                libraryVersion: `OpenSSL/${process.versions.openssl}`,
                interfaceVersion: `NodeJS/${process.version}`,
                // Getting sessionId is currently impossible from the request, as sessionId is not related in any way to socket
                // In NodeJS sessionId is only available to 'newSession', 'resumeSession' events, but those are emitted on the server
                // for obvious reasons, so sessionId cannot be linked to the socket...
                // The only way to obtain the sessionId is to add 'connection' event to the server after the default listener
                // and replace some inner methods of the TLSWrap that would be called during session setup.
                // However, maching the HTTP2Session and the SSLSession is not part of the FCGI library, we add this here
                // to allow the user to decide how to get the session...
                sessionId: null,
                sessionResumed: null,
                allowRenegotiate: null,
                cipherIsExport: null,
                cipherUseKeySize: null,
                cipherAlgorithmKeySize: null
            };
            defineCacheGetter(defaults, 'serverName', () => {
                return request.socket.encrypted && request.socket.servername;
            });
            defineCacheGetter(defaults, 'protocol', () => {
                return typeof request.socket.getProtocol === 'function' ? request.socket.getProtocol() : null;
            });
            defineCacheGetter(defaults, 'cipher', () => {
                if (typeof request.socket.getCipher === 'function') {
                    const cipherData = request.socket.getCipher();
                    if (cipherData != null) {
                        return cipherData.name;
                    }
                }
            });
            defineCacheGetter(defaults, 'serverCertificate', () => {
                return typeof request.socket.getCertificate === 'function' ? request.socket.getCertificate() : null;
            });
            defineCacheGetter(defaults, 'clientCertificate', () => {
                return typeof request.socket.getPeerCertificate === 'function' ? request.socket.getPeerCertificate(true) : null;
            });
            Object.setPrototypeOf(options, defaults);
            if (options.serverName != null) {
                parameters.SSL_TLS_SNI = options.serverName;
            }
            if (options.protocol != null) {
                parameters.SSL_PROTOCOL = options.protocol;
            }
            if (options.cipher != null) {
                parameters.SSL_CIPHER = options.cipher;
            }
            if (options.sessionId != null) {
                parameters.SSL_SESSION_ID = options.sessionId;
            }
            if (options.sessionResumed != null) {
                parameters.SSL_SESSION_RESUMED = options.sessionResumed ? 'Resumed' : 'Initial';
            }
            if (options.allowRenegotiate != null) {
                parameters.SSL_SECURE_RENEG = options.allowRenegotiate ? 'true' : 'false';
            }
            if (options.cipherIsExport === true) {
                parameters.SSL_CIPHER_EXPORT = 'true';
            }
            if (options.cipherUseKeySize != null) {
                parameters.SSL_CIPHER_USEKEYSIZE = options.cipherUseKeySize;
            }
            if (options.cipherAlgorithmKeySize != null) {
                parameters.SSL_CIPHER_ALGKEYSIZE = options.cipherAlgorithmKeySize;
            }
            if (options.libraryVersion != null) {
                parameters.SSL_VERSION_LIBRARY = options.libraryVersion;
            }
            if (options.interfaceVersion != null) {
                parameters.SSL_VERSION_INTERFACE = options.interfaceVersion;
            }
            // TODO: Process client/server certificate information
        }

        getScriptFilename(filename) {
            if (!path.isAbsolute(filename)) {
                path.resolve(process.cwd(), filename);
            }
            if (path.sep !== '/') {
                filename = path.split(path.sep).join('/');
            }
            const urlParameters = {
                protocol: 'proxy:fcgi:',
                slashes: true,
                pathname: filename
            };
            if (this[symbols.options].path) {
                urlParameters.host = 'localhost';
            } else {
                urlParameters.hostname = this[symbols.options].host;
                urlParameters.port = this[symbols.options].port;
            }
            return url.format(urlParameters);
        }

        [symbols._connect]() {
            const options = {};
            if (typeof this[symbols.options].path === 'string' && this[symbols.options].path.length > 0) {
                options.path = this[symbols.options].path;
            } else {
                options.host = this[symbols.options].host;
                options.port = this[symbols.options].port;
            }
            return net.createConnection(options);
        }

        [symbols._loadHeaders](target, source) {
            for (const headerName in source) {
                if (Object.hasOwnProperty.call(source, headerName)) {
                    if (typeof source[headerName] === 'function') {
                        target[headerName] = source[headerName].call(this, target[headerName], headerName);
                    } else {
                        target[headerName] = source[headerName];
                    }
                }
            }
        }
    }

    FCGIConnection.ProtocolStatusError = class ProtocolStatusError extends Error {
        constructor(message, status) {
            super(message);
            this.message = message;
            this.code = status;
            this.name = 'FCGIConnection.ProtocolStatusError';
            Error.captureStackTrace(this, this.constructor);
        }
    };

    function parseIPv6(ip) {
        ip = ip.split('::');
        let g = 0;
        for (let i = 0; i < ip.length; ++i) {
            ip[i] = ip[i].split(':');
            g += ip[i].length;
        }
        const ip4 = /([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)/.exec(ip[ip.length - 1][ip[ip.length - 1].length - 1]);
        if (ip4) {
            ip[ip.length - 1].pop();
            ip[ip.length - 1].push(ip4[1] * 256 + (ip4[2] | 0));
            ip[ip.length - 1].push(ip4[3] * 256 + (ip4[4] | 0));
        }
        if (ip.length === 2 && g < 8) {
            if (ip[0].length === 1 && ip[0][0] === '') {
                ip[0].length = 0;
            }
            ip[2] = ip[1];
            ip[1] = [];
            while (g < 8) {
                ip[1].push(0);
                ++g;
            }
        }
        ip = Array.prototype.concat.apply([], ip);
        if (ip.length !== 8) {
            return null;
        }
        for (let i = 0; i < ip.length; ++i) {
            ip[i] = typeof ip[i] === 'number' ? ip[i] : ip[i].length > 0 ? parseInt(ip[i], 16) : 0;
            if (!isFinite(ip[i])) {
                return null;
            }
        }
        return ip;
    }

    function getIP(ip) {
        if (net.isIPv6(ip)) {
            const address = parseIPv6(ip);
            if (address[0] === 0 && address[1] === 0 && address[2] === 0 && address[3] === 0 && address[4] === 0 && address[5] === 65535) {
                // IP is actually IPv4 encoded as IPv6, so we extract it
                return (address[6] >> 8) + '.' + (address[6] & 0xFF) + '.' + (address[7] >> 8) + '.' + (address[7] & 0xFF);
            }
        }
        return ip;
    }

    function defineCacheGetter(object, name, getter) {
        Object.defineProperty(object, name, {
            configurable: true,
            enumerable: true,
            get: () => {
                const value = getter.call(object);
                Object.defineProperty(object, name, {
                    configurable: true,
                    enumerable: true,
                    writable: true,
                    value
                });
                return value;
            }
        });
    }

    module.exports = FCGIConnection;
})();
