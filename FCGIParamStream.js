(function () {
    'use strict';

    const stream = require('stream');

    const symbols = {
        names: Symbol('names'),
        values: Symbol('values'),
        currentIndex: Symbol('currentIndex'),
        currentBuffer: Symbol('currentBuffer'),
        currentOffset: Symbol('currentOffset'),
        _addNameValuePair: Symbol('_addNameValuePair')
    };

    class FCGIParamStream extends stream.Readable {
        constructor(parameters, options = {}) {
            super({
                ...{
                    highWaterMark: 65528,
                    emitClose: true,
                    autoDestroy: true
                },
                ...options,
                ...{
                    objectMode: false,
                    encoding: null
                }
            });
            this[symbols.names] = [];
            this[symbols.values] = [];
            this[symbols.currentIndex] = 0;
            this[symbols.currentOffset] = 0;
            this[symbols.currentBuffer] = null;
            if (parameters instanceof Map) {
                for (const [name, value] of parameters) {
                    this[symbols._addNameValuePair](name, value, options.encoding);
                }
            } else if (parameters != null && typeof parameters === 'object') {
                for (const name in parameters) {
                    if (Object.hasOwnProperty.call(parameters, name)) {
                        this[symbols._addNameValuePair](name, parameters[name], options.encoding);
                    }
                }
            }
        }

        _read(size) {
            let bytesWritten = 0;
            while (bytesWritten < size && this[symbols.currentIndex] < this[symbols.names].length) {
                if (this[symbols.currentBuffer] == null) {
                    this[symbols.currentBuffer] = makeBuffer(this[symbols.names][this[symbols.currentIndex]], this[symbols.values][this[symbols.currentIndex]]);
                    this[symbols.currentOffset] = 0;
                }
                const bytesToWrite = Math.min(size - bytesWritten, this[symbols.currentBuffer].length - this[symbols.currentOffset]);
                const pushResult = this.push(this[symbols.currentBuffer].slice(this[symbols.currentOffset], this[symbols.currentOffset] + bytesToWrite));
                bytesWritten += bytesToWrite;
                this[symbols.currentOffset] += bytesToWrite;
                if (this[symbols.currentOffset] >= this[symbols.currentBuffer].length) {
                    this[symbols.currentBuffer] = null;
                    ++this[symbols.currentIndex];
                }
                if (!pushResult) {
                    return;
                }
            }
            if (this[symbols.currentIndex] >= this[symbols.names].length) {
                this.push(null); // Signal end of stream
            }
        }

        [symbols._addNameValuePair](name, value, encoding = 'utf8') {
            if (typeof name === 'string') {
                if (name.length <= 0) {
                    throw new FCGIParamStream.InvalidParameter('Parameter name cannot be empty');
                }
                name = Buffer.from(name, encoding);
            }
            if (!Buffer.isBuffer(name)) {
                throw new FCGIParamStream.InvalidParameter('Parameter name must be string or buffer');
            }
            if (value == null) {
                value = Buffer.allocUnsafe(0);
            }
            if (typeof value === 'boolean') {
                value = 0 | value;
            }
            if (typeof value === 'number') {
                if (!isFinite(value)) {
                    throw new FCGIParamStream.InvalidParameter(`Parameter [${name}] invalid: only finite numbers are supported`);
                }
                if (value === parseInt(value)) {
                    value = value.toString(10);
                } else {
                    value = value.toFixed(6);
                }
            }
            if (typeof value === 'string') {
                value = Buffer.from(value, encoding);
            }
            if (!Buffer.isBuffer(name)) {
                throw new FCGIParamStream.InvalidParameter(`Parameter [${name}] invalid: expected primitive value or buffer`);
            }
            this[symbols.names].push(name);
            this[symbols.values].push(value);
        }
    }

    FCGIParamStream.InvalidParameter = class InvalidParameter extends Error {
        constructor(message) {
            super(message);
            Error.captureStackTrace(this, this.constructor);
        }
    };

    function makeBuffer(name, value) {
        let offset = name.length + value.length;
        for (const key of arguments) {
            offset += key.length > 127 ? 4 : 1;
        }
        const buffer = Buffer.allocUnsafe(offset);
        offset = 0;
        for (const key of arguments) {
            if (key.length > 127) {
                buffer.writeUInt32BE(key.length, offset);
                buffer.writeUInt8(buffer.readUInt8(offset) | 0x80, offset);
                offset += 4;
            } else {
                buffer.writeUInt8(key.length, offset++);
            }
        }
        name.copy(buffer, offset);
        offset += name.length;
        value.copy(buffer, offset);
        return buffer;
    }

    module.exports = FCGIParamStream;
})();
