(function () {
    'use strict';

    const stream = require('stream');

    const constants = require('./constants');

    const symbols = {
        buffer: Symbol('buffer'),
        packet: Symbol('packet'),
        offset: Symbol('offset'),
        onPacket: Symbol('onPacket'),
        onFinish: Symbol('onFinish'),
        _asyncWrite: Symbol('_asyncWrite'),
        _asyncFinal: Symbol('_asyncFinal')
    };

    class FCGIOutputStream extends stream.Writable {
        constructor(onPacket, onFinish, options = {}) {
            super({
                ...{
                    highWaterMark: 65536,
                    emitClose: true,
                    autoDestroy: true
                },
                ...options,
                ...{
                    objectMode: false,
                    decodeStrings: true
                }
            });
            if (typeof onPacket !== 'function' || typeof onFinish !== 'function') {
                throw new TypeError('Expected arguments 1 and 2 to be functions');
            }
            this[symbols.onPacket] = onPacket;
            this[symbols.onFinish] = onFinish;
            this[symbols.buffer] = null;
            this[symbols.packet] = null;
            this[symbols.offset] = 0;
        }

        _write(chunk, encoding, callback) {
            this[symbols._asyncWrite](chunk).then(() => void callback(), callback);
        }

        _final(callback) {
            this[symbols._asyncFinal]().then(() => void callback(), callback);
        }

        async [symbols._asyncWrite](chunk) {
            if (this[symbols.buffer] != null) {
                chunk = Buffer.concat([this[symbols.buffer], chunk], this[symbols.buffer].length + chunk.length);
                this[symbols.buffer] = null;
            }
            let offset = 0;
            while (offset < chunk.length) {
                if (this[symbols.packet] == null) {
                    if (chunk.length - offset < constants.FCGI_HEADER_SIZE) {
                        this[symbols.buffer] = chunk.slice(offset);
                        return;
                    }
                    const packet = {};
                    packet.version = chunk.readUInt8(offset);
                    if (packet.version !== 1) {
                        throw new FCGIOutputStream.InvalidData(`Invalid version: expected ${constants.FCGI_VERSION}, got ${packet.version}`);
                    }
                    packet.type = chunk.readUInt8(offset + 1);
                    if (packet.type < 1 || packet.type > 11) {
                        throw new FCGIOutputStream.InvalidData(`Invalid type: expected 1-${constants.fcgiTypes.FCGI_MAX_TYPE}, got ${packet.type}`);
                    }
                    packet.requestId = chunk.readUInt16BE(offset + 2);
                    packet.contentLength = chunk.readUInt16BE(offset + 4);
                    packet.padding = chunk.readUInt8(offset + 6);
                    packet.buffer = Buffer.allocUnsafe(constants.FCGI_HEADER_SIZE + packet.contentLength + packet.padding);
                    const currentLength = this[symbols.offset] = Math.min(chunk.length - offset, constants.FCGI_HEADER_SIZE + packet.contentLength + packet.padding);
                    chunk.copy(packet.buffer, 0, offset, offset + currentLength);
                    offset += currentLength;
                    this[symbols.packet] = packet;
                } else {
                    const currentLength = Math.min(chunk.length - offset, this[symbols.packet].buffer.length - this[symbols.offset]);
                    chunk.copy(this[symbols.packet].buffer, this[symbols.offset], offset, offset + currentLength);
                    offset += currentLength;
                    this[symbols.offset] += currentLength;
                }
                if (this[symbols.offset] >= this[symbols.packet].buffer.length) {
                    const packet = this[symbols.packet];
                    this[symbols.packet] = null;
                    this[symbols.offset] = 0;
                    await this[symbols.onPacket](packet);
                }
            }
        }

        async [symbols._asyncFinal]() {
            if (this[symbols.buffer] != null || this[symbols.packet] != null) {
                throw new FCGIOutputStream.InvalidData('Unexpected end of stream during partial package processing');
            }
            await this[symbols.onFinish]();
        }
    }

    FCGIOutputStream.InvalidData = class InvalidData extends Error {
        constructor(message) {
            super();
            this.message = message;
            this.name = 'FCGIOutputStream.InvalidData';
            Error.captureStackTrace(this, this.constructor);
        }
    };

    module.exports = FCGIOutputStream;
})();
