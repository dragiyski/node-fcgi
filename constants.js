exports.fcgiTypes = {
    FCGI_BEGIN_REQUEST: 1,
    FCGI_ABORT_REQUEST: 2,
    FCGI_END_REQUEST: 3,
    FCGI_PARAMS: 4,
    FCGI_STDIN: 5,
    FCGI_STDOUT: 6,
    FCGI_STDERR: 7,
    FCGI_DATA: 8,
    FCGI_GET_VALUES: 9,
    FCGI_GET_VALUES_RESULT: 10,
    FCGI_UNKNOWN_TYPE: 11,
    FCGI_MAX_TYPE: 11
};

exports.fcgiTypeNames = {
    1: 'FCGI_BEGIN_REQUEST',
    2: 'FCGI_ABORT_REQUEST',
    3: 'FCGI_END_REQUEST',
    4: 'FCGI_PARAMS',
    5: 'FCGI_STDIN',
    6: 'FCGI_STDOUT',
    7: 'FCGI_STDERR',
    8: 'FCGI_DATA',
    9: 'FCGI_GET_VALUES',
    10: 'FCGI_GET_VALUES_RESULT',
    11: 'FCGI_UNKNOWN_TYPE'
};

exports.fcgiRoles = {
    FCGI_RESPONDER: 1,
    FCGI_AUTHORIZER: 2,
    FCGI_FILTER: 3,
    FCGI_MAX_ROLE: 3
};

exports.fcgiRoleNames = {
    1: 'FCGI_RESPONDER',
    2: 'FCGI_AUTHORIZER',
    3: 'FCGI_FILTER'
};

exports.fcgiProtocolStatus = {
    FCGI_REQUEST_COMPLETE: 0,
    FCGI_CANT_MPX_CONN: 1,
    FCGI_OVERLOADED: 2,
    FCGI_UNKNOWN_ROLE: 3,
    FCGI_MAX_PROTOCOL_STATUS: 3
};

exports.fcgiProtocolStatusNames = {
    0: 'FCGI_REQUEST_COMPLETE',
    1: 'FCGI_CANT_MPX_CONN',
    2: 'FCGI_OVERLOADED',
    3: 'FCGI_UNKNOWN_ROLE'
};

exports.fcgiBeginRequestFlags = {
    FCGI_KEEP_CONN: 1
};

exports.fcgiBeginRequestFlagNames = {
    1: 'FCGI_KEEP_CONN'
};

exports.FCGI_HEADER_SIZE = 8;
exports.FCGI_BEGIN_REQUEST_SIZE = 8;

exports.FCGI_VERSION = 1;
