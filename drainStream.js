(function () {
    'use strict';

    module.exports = async function drainStream(stream) {
        return new Promise((resolve, reject) => {
            const onDrain = () => {
                stream.off('error', onError);
                resolve(stream);
            };
            const onError = error => {
                stream.off('drain', onDrain);
                reject(error);
            };
            stream.once('drain', onDrain).once('error', onError);
        });
    };
})();
