(function () {
    'use strict';

    const stream = require('stream');

    const symbols = {
        buffer: Symbol('buffer'),
        readOffset: Symbol('readOffset'),
        writeOffset: Symbol('writeOffset'),
        error: Symbol('error'),
        flushing: Symbol('flushing'),
        canPush: Symbol('canPush'),
        readSize: Symbol('readSize'),
        writeJob: Symbol('writeJob'),
        writeFinish: Symbol('writeFinish'),
        _sendBuffer: Symbol('_sendBuffer'),
        _flushWriteJob: Symbol('_flushWriteJob')
    };

    class TCPAlignStream extends stream.Duplex {
        constructor(length = 65536, options) {
            super({
                ...{
                    highWaterMark: length,
                    defaultEncoding: 'utf8',
                    emitClose: true,
                    autoDestroy: true
                },
                ...options,
                ...{
                    objectMode: false,
                    decodeStrings: true
                }
            });
            this[symbols.buffer] = Buffer.allocUnsafe(length);
            this[symbols.readOffset] = this[symbols.writeOffset] = 0;
            this[symbols.canPush] = this[symbols.flushing] = false;
            this[symbols.readSize] = 0;
        }

        flush() {
            // last this.push() might returned false, so we are awaiting _read call. In this case _sendBuffer is no-op,
            // so we set a flag to flush the buffer immediately on the next read call
            this[symbols.flushing] = true;
            // alternatively, past _read call that received no push (since we do not _sendBuffer, unless there is
            // enough aggregated data), can be send immediately.
            this[symbols._sendBuffer]();
            // If there is no data to be send, _sendBuffer just reset the flushing flag, resulting in no-op.
        }

        _read(size) {
            this[symbols.canPush] = true;
            this[symbols.readSize] += size;
            if (this[symbols.buffer] != null && (this[symbols.flushing] || this[symbols.writeFinish] != null || this[symbols.writeOffset] >= this[symbols.buffer].length)) {
                this[symbols._sendBuffer]();
            }
        }

        _destroy(error, callback) {
            if (error) {
                if (this[symbols.writeJob] != null) {
                    const callback = this[symbols.writeJob].callback;
                    this[symbols.writeJob] = null;
                    callback(error);
                }
                if (typeof this[symbols.writeFinish] === 'function') {
                    const callback = this[symbols.writeFinish];
                    this[symbols.writeFinish] = true;
                    callback(error);
                }
                this[symbols.error] = error;
            }
            callback(error);
        }

        _write(chunk, encoding, callback) {
            if (this[symbols.error]) {
                return void callback(this[symbols.error]);
            }
            const bytesToCopy = Math.min(this[symbols.buffer].length - this[symbols.writeOffset], chunk.length);
            if (bytesToCopy > 0) {
                chunk.copy(this[symbols.buffer], this[symbols.writeOffset], 0, bytesToCopy);
                this[symbols.writeOffset] += bytesToCopy;
                if (this[symbols.writeOffset] >= this[symbols.buffer].length) {
                    this[symbols._sendBuffer]();
                }
            }
            if (bytesToCopy < chunk.length) {
                this[symbols.writeJob] = {
                    buffer: chunk,
                    offset: bytesToCopy,
                    callback
                };
            } else {
                callback();
            }
        }

        _final(callback) {
            if (this[symbols.error]) {
                return void callback(this[symbols.error]);
            }
            this[symbols.writeFinish] = callback;
            this.flush();
        }

        [symbols._sendBuffer]() {
            if (!this[symbols.canPush]) {
                return;
            }
            this[symbols.flushing] = false;
            // writeOffset is buffer.length most of the time, except when this is a forced flush
            const bytesToSend = Math.min(this[symbols.writeOffset] - this[symbols.readOffset], this[symbols.readSize]);
            if (bytesToSend <= 0) {
                if (this[symbols.writeFinish]) {
                    // No more writes are coming in and no data to send, so singal EOF
                    this.push(null);
                }
                return;
            }
            this[symbols.canPush] = this.push(this[symbols.buffer].slice(this[symbols.readOffset], this[symbols.readOffset] + bytesToSend));
            this[symbols.readOffset] += bytesToSend;
            this[symbols.readSize] -= bytesToSend;
            if (this[symbols.readOffset] >= this[symbols.writeOffset]) {
                // all data has been pushed, reset values:
                this[symbols.readOffset] = this[symbols.writeOffset] = 0;
                this[symbols._flushWriteJob]();
            }
        }

        [symbols._flushWriteJob]() {
            if (this[symbols.writeJob] != null) {
                const job = this[symbols.writeJob];
                const bytesToCopy = Math.min(this[symbols.buffer].length - this[symbols.writeOffset], job.buffer.length - job.offset);
                job.buffer.copy(this[symbols.buffer], this[symbols.writeOffset], job.offset, job.offset + bytesToCopy);
                job.offset += bytesToCopy;
                this[symbols.writeOffset] += bytesToCopy;
                if (job.offset >= job.buffer.length) {
                    this[symbols.writeJob] = null;
                    const callback = job.callback;
                    callback();
                    if (typeof this[symbols.writeFinish] === 'function') {
                        const callback = this[symbols.writeFinish];
                        this[symbols.writeFinish] = true;
                        callback();
                    }
                }
                if (this[symbols.writeOffset] >= this[symbols.buffer].length) {
                    this[symbols._sendBuffer]();
                }
            }
        }
    }

    module.exports = TCPAlignStream;
})();
