(function () {
    'use strict';

    const stream = require('stream');

    const symbols = {
        key: Symbol('key'),
        value: Symbol('value'),
        state: Symbol('state'),
        hadHeader: Symbol('hadHeader'),
        _transform: Symbol('_transform')
    };

    class HTTPStream extends stream.Transform {
        constructor() {
            super({
                objectMode: false,
                decodeStrings: true,
                autoDestroy: true
            });
            this[symbols.state] = 1;
            this[symbols.hadHeader] = false;
        }

        [symbols._transform](buffer) {
            let offset = 0;
            while (offset < buffer.length) {
                switch (this[symbols.state]) {
                    case 0:
                        this.push(buffer.slice(offset));
                        offset = buffer.length;
                        break;
                    case 1:
                        this[symbols.key] = '';
                        this[symbols.value] = [];
                        this[symbols.state] = 2;
                        break;
                    case 2: {
                        const charCode = buffer[offset++];
                        if (charCode === 58) { // :
                            this[symbols.state] = 3;
                        } else if (charCode >= 32 && charCode <= 126) {
                            this[symbols.key] += String.fromCharCode(charCode);
                        } else if (charCode === 13) {
                            this[symbols.state] = 4;
                        } else if (charCode === 10) {
                            this[symbols.state] = 5;
                        } else {
                            throw new HTTPStream.InvalidData(`Unexpected byte data with code [${charCode}] in header field name: only ascii characters should appear in the header field name`);
                        }
                        break;
                    }
                    case 3: {
                        const charCode = buffer[offset++];
                        if (charCode === 13) {
                            this[symbols.state] = 4;
                        } else if (charCode === 10) {
                            // Usually this should not happen, as standard says newline will be 13/10 pair.
                            this[symbols.state] = 5;
                        } else {
                            this[symbols.value].push(charCode);
                        }
                        break;
                    }
                    case 4: {
                        const charCode = buffer[offset++];
                        if (charCode !== 10) {
                            throw new HTTPStream.InvalidData(`Expected newline to consist of [13, 10] pair, the second byte is ${charCode}`);
                        }
                        this[symbols.state] = 5;
                        break;
                    }
                    case 5: {
                        if (this[symbols.key].length <= 0) {
                            if (!this[symbols.hadHeader]) {
                                throw new HTTPStream.InvalidData(`Unexpected newline at the beginning of the stream`);
                            }
                            this.emit('body');
                            this[symbols.state] = 0;
                            break;
                        }
                        let charCode = buffer[offset];
                        // Handle (deprecated) line continuation
                        if (charCode === 32 || charCode === 9) {
                            while (charCode === 32 || charCode === 9) {
                                charCode = buffer[++offset];
                            }
                            this[symbols.state] = 3;
                            break;
                        }
                        this[symbols.hadHeader] = true;
                        this[symbols.key] = this[symbols.key].toLowerCase();
                        this[symbols.value] = Buffer.from(this[symbols.value]).toString('utf8').trim();
                        if (this[symbols.key] === 'status') {
                            let statusMessage = this[symbols.value];
                            if (!/^[0-9]+/.test(statusMessage)) {
                                throw new HTTPStream.InvalidData(`Expected status header to begin with a number`);
                            }
                            const statusCode = parseInt(statusMessage, 10);
                            statusMessage = statusMessage.substr(('' + statusCode).length).trim();
                            this.emit('status', statusCode, statusMessage);
                        } else {
                            this.emit('header', this[symbols.key], this[symbols.value]);
                        }
                        this[symbols.state] = 1;
                        break;
                    }
                }
            }
        }

        _transform(chunk, encoding, callback) {
            try {
                this[symbols._transform](chunk);
            } catch (e) {
                callback(e);
                return;
            }
            callback();
        }
    }

    HTTPStream.InvalidData = class InvalidData extends Error {
        constructor(message) {
            super();
            this.message = message;
            this.name = 'HTTPStream.InvalidData';
            Error.captureStackTrace(this, this.constructor);
        }
    };

    module.exports = HTTPStream;
})();
