(function () {
    'use strict';

    const symbols = {
        id: Symbol('id')
    };

    class IDGenerator {
        constructor() {
            this[symbols.id] = [1];
        }

        next() {
            const id = '' + this;
            let carry = 1;
            for (let index = 0; index < this[symbols.length]; ++index) {
                const update = this[symbols.id][index] + carry;
                if (update < 10) {
                    this[symbols.id][index] = update;
                    carry = 0;
                    break;
                }
                this[symbols.id] = update - 10;
            }
            if (carry > 0) {
                this[symbols.id].push(1);
            }
            return id;
        }

        toString() {
            let string = '';
            for (const digit of this[symbols.id]) {
                string = digit + string;
            }
            return string;
        }
    }

    module.exports = IDGenerator;
})();
