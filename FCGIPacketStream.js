(function () {
    'use strict';

    const stream = require('stream');

    const constants = require('./constants');

    const symbols = {
        currentPacket: Symbol('currentPacket'),
        currentReadOffset: Symbol('currentReadOffset'),
        currentWriteOffset: Symbol('currentWriteOffset'),
        maxPaddingLength: Symbol('maxPaddingLength'),
        packetSize: Symbol('packetSize'),
        alignment: Symbol('alignment'),
        writeJob: Symbol('writeJob'),
        error: Symbol('error'),
        requestId: Symbol('requestId'),
        readingSize: Symbol('readingSize'),
        writeLimit: Symbol('writeLimit'),
        type: Symbol('type'),
        finishing: Symbol('finishing'),
        _finishPacket: Symbol('_finishPacket'),
        _drainPacket: Symbol('_drainPacket'),
        _resetPacket: Symbol('_resetPacket'),
        _flushPendingWrite: Symbol('_flushPendingWrite')
    };

    class FCGIPacketStream extends stream.Duplex {
        constructor(options = {}, streamOptions = {}) {
            options = {
                ...{
                    requestId: 1,
                    packetSize: 65536,
                    alignment: 8
                },
                ...options
            };
            super({
                ...{
                    highWaterMark: 65536,
                    defaultEncoding: 'utf8',
                    emitClose: true,
                    autoDestroy: true
                },
                ...streamOptions,
                ...{
                    objectMode: false,
                    decodeStrings: true
                }
            });
            if (options.type !== parseInt(options.type) || options.type < 0 || options.type > 255) {
                throw new Error(`Option [type] must be valid integer between 0 and 255`);
            }
            if (options.requestId !== parseInt(options.requestId) || options.requestId < 0 || options.requestId > 65535) {
                throw new Error(`Option [requestId] must be valid integer between 0 and 65535.`);
            }
            if (options.packetSize !== parseInt(options.packetSize) || options.packetSize < 16) {
                throw new Error(`Option [packetSize] ${options.packetSize} is invalid or too small, expected minimum 16 bytes.`);
            }
            if (options.alignment === 0) {
                options.alignment = 1;
            } else if (options.alignment !== parseInt(options.alignment) || options.alignment < 0 || options.alignment > options.packetSize || options.alignment > 255) {
                throw new Error(`Option [alignment] must be positive integer less than or equal to packet size and within [0-255] range.`);
            }
            let maxPaddingLength = options.packetSize % options.alignment;
            if (maxPaddingLength > 0) {
                maxPaddingLength = options.alignment - maxPaddingLength;
                if (maxPaddingLength > 255) {
                    throw new Error('Invalid alignment + packetSize combination: the maximum padding for this alignment exceeds 255 bytes');
                }
            }
            this[symbols.type] = options.type;
            this[symbols.requestId] = options.requestId;
            this[symbols.packetSize] = options.packetSize;
            this[symbols.alignment] = options.alignment;
            this[symbols.maxPaddingLength] = maxPaddingLength;
            this[symbols.currentPacket] = Buffer.allocUnsafe(this[symbols.packetSize] + this[symbols.maxPaddingLength]);
            this[symbols.currentReadOffset] = 0;
            this[symbols.currentWriteOffset] = constants.FCGI_HEADER_SIZE;
            this[symbols.writeJob] = null;
            this[symbols.readingSize] = 0;
            this[symbols.writeLimit] = 0;
            this[symbols.finishing] = null;
        }

        _write(chunk, encoding, callback) {
            try {
                if (this[symbols.error] != null) {
                    return void callback(this[symbols.error]);
                }
                const bytesToCopy = Math.min(chunk.length, this[symbols.packetSize] - this[symbols.currentWriteOffset]);
                chunk.copy(this[symbols.currentPacket], this[symbols.currentWriteOffset], 0, bytesToCopy);
                this[symbols.currentWriteOffset] += bytesToCopy;
                if (bytesToCopy < chunk.length) {
                    this[symbols.writeJob] = {
                        buffer: chunk,
                        offset: bytesToCopy,
                        callback
                    };
                } else {
                    callback();
                }
                if (this[symbols.currentWriteOffset] >= this[symbols.packetSize]) {
                    this[symbols._finishPacket]();
                }
            } catch (e) {
                this[symbols.error] = e;
                callback(e);
            }
        }

        _final(callback) {
            // A neat trick to write additional empty packet at the end
            this[symbols.finishing] = err => {
                if (err) {
                    this[symbols.finishing] = true;
                    return void callback(err);
                }
                this[symbols.finishing] = err => {
                    if (err) {
                        this[symbols.finishing] = true;
                        return void callback(err);
                    }
                    if (this[symbols.readingSize] > 0) {
                        this.push(null);
                    }
                    this[symbols.finishing] = true;
                    this[symbols.currentPacket] = null;
                    callback();
                };
                this[symbols._resetPacket]();
                this[symbols._finishPacket]();
            };
            if (this[symbols.writeLimit] > 0) {
                // If we have something on the "read" side to push, drain it...
                this[symbols._drainPacket]();
            } else if (this[symbols.currentWriteOffset] > constants.FCGI_HEADER_SIZE) {
                // If there is is data on the "write" side, flush it, even if it is less than the max buffer size
                this[symbols._finishPacket]();
            } else {
                // If no data, just finish the stream... this can occur if the stream is empty of data is divisible by packetSize - FCGI_HEADER_SIZE.
                const callback = this[symbols.finishing];
                callback();
            }
        }

        _destroy(error, callback) {
            this[symbols.error] = error;
            this[symbols.currentPacket] = null;
            this[symbols.writeLimit] = this[symbols.readingSize] = this[symbols.currentReadOffset] = 0;
            if (error) {
                if (this[symbols.writeJob] != null) {
                    const callback = this[symbols.writeJob].callback;
                    this[symbols.writeJob] = null;
                    callback(error);
                }
                if (typeof this[symbols.finishing] === 'function') {
                    const callback = this[symbols.finishing];
                    callback(error);
                }
            }
            callback(error);
        }

        _read(size) {
            try {
                this[symbols.readingSize] += size;
                if (this[symbols.writeLimit] > 0 || typeof this[symbols] === 'function') {
                    this[symbols._drainPacket]();
                } else if (this[symbols.finishing] === true) {
                    this.push(null);
                }
            } catch (e) {
                this.destroy(e);
            }
        }

        [symbols._finishPacket]() {
            this[symbols.currentPacket].writeUInt8(constants.FCGI_VERSION, 0);
            this[symbols.currentPacket].writeUInt8(this[symbols.type], 1);
            this[symbols.currentPacket].writeUInt16BE(this[symbols.requestId], 2);
            this[symbols.currentPacket].writeUInt16BE(this[symbols.currentWriteOffset] - constants.FCGI_HEADER_SIZE, 4);
            let paddingLength = this[symbols.currentWriteOffset] % this[symbols.alignment];
            if (paddingLength > 0) {
                paddingLength = this[symbols.alignment] - paddingLength;
            }
            this[symbols.currentPacket].writeUInt8(paddingLength, 6);
            this[symbols.currentPacket].writeUInt8(0, 7);
            if (paddingLength > 0) {
                this[symbols.currentPacket].fill(0, this[symbols.currentWriteOffset], this[symbols.currentWriteOffset] + paddingLength);
            }
            this[symbols.writeLimit] = this[symbols.currentWriteOffset] + paddingLength;
            this[symbols.currentReadOffset] = 0;
            this[symbols._drainPacket]();
        }

        [symbols._drainPacket]() {
            while (this[symbols.readingSize] > 0 && this[symbols.currentReadOffset] < this[symbols.writeLimit]) {
                const bytesToCopy = Math.min(this[symbols.readingSize], this[symbols.writeLimit] - this[symbols.currentReadOffset]);
                const pushResult = this.push(this[symbols.currentPacket].slice(this[symbols.currentReadOffset], this[symbols.currentReadOffset] + bytesToCopy));
                this[symbols.currentReadOffset] += bytesToCopy;
                this[symbols.readingSize] -= bytesToCopy;
                if (!pushResult) {
                    break;
                }
            }
            if (this[symbols.currentReadOffset] >= this[symbols.writeLimit]) {
                if (typeof this[symbols.finishing] === 'function') {
                    const callback = this[symbols.finishing];
                    callback();
                } else {
                    this[symbols._resetPacket]();
                }
            }
        }

        [symbols._resetPacket]() {
            this[symbols.writeLimit] = 0;
            this[symbols.currentWriteOffset] = constants.FCGI_HEADER_SIZE;
            this[symbols.currentReadOffset] = 0;
            this[symbols._flushPendingWrite]();
        }

        [symbols._flushPendingWrite]() {
            if (this[symbols.writeJob] != null) {
                if (this[symbols.error]) {
                    const callback = this[symbols.writeJob].callback;
                    this[symbols.writeJob] = null;
                    callback(this[symbols.error]);
                }
                const bytesToCopy = Math.min(this[symbols.writeJob].buffer.length - this[symbols.writeJob].offset, this[symbols.packetSize] - this[symbols.currentWriteOffset]);
                this[symbols.writeJob].buffer.copy(this[symbols.currentPacket], this[symbols.currentWriteOffset], this[symbols.writeJob].offset, this[symbols.writeJob].offset + bytesToCopy);
                this[symbols.writeJob].offset += bytesToCopy;
                this[symbols.currentWriteOffset] += bytesToCopy;
                if (this[symbols.writeJob].offset >= this[symbols.writeJob].buffer.length) {
                    const callback = this[symbols.writeJob].callback;
                    this[symbols.writeJob] = null;
                    callback();
                }
                if (this[symbols.currentWriteOffset] >= this[symbols.packetSize] || this[symbols.finishing]) {
                    this[symbols._finishPacket]();
                }
            }
        }
    }

    module.exports = FCGIPacketStream;
})();
